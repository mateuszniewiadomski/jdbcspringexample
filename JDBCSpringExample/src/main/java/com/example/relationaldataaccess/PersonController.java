package com.example.relationaldataaccess;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.util.List;

@RestController
public class PersonController {

    DataSource dataSource;
    JdbcTemplate jdbcTemplate;
    PersonRepository pr;


    @Autowired
    public PersonController(DataSource dataSource, JdbcTemplate jdbcTemplate, PersonRepository pr) {
        this.dataSource = dataSource;
        this.jdbcTemplate = jdbcTemplate;
        this.pr = pr;
    }

    @GetMapping("/")
    public List<Person> viewAll() {
        jdbcTemplate.setDataSource(dataSource);
        return  jdbcTemplate.query(
                "SELECT * FROM customers",
                (rs, rowNum) -> new Person(rs.getString("id"), rs.getString("first_name"), rs.getString("last_name"))
        );
    }

    @GetMapping("/{id}")
    public List<Person> viewById(@PathVariable("id") long id) { return pr.findByID(id); }

    @PostMapping("/new")
    public String newPerson(String first_name, String last_name) {
        pr.save(first_name, last_name);
        return "added: " + first_name + " " + last_name ;
    }

}
