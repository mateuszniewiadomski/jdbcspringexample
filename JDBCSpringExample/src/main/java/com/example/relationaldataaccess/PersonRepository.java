package com.example.relationaldataaccess;

import java.util.List;

public interface PersonRepository {

    Iterable<Person> findAll();

    List<Person> findByID(long id);

    void save(String first_name, String last_name);
}
