package com.example.relationaldataaccess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RawJdbcPersonRepository implements PersonRepository {

    //private DataSource dataSource;

    private JdbcTemplate jdbc;

            /*
    public RawJdbcPersonRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    */

    @Autowired
    public RawJdbcPersonRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    private Person mapRowToPerson(ResultSet rs, int rowNum)
            throws SQLException {
        return new Person(
                rs.getString("id"),
                rs.getString("first_name"),
                rs.getString("last_name"));
    }

    @Override
    public Iterable<Person> findAll() {
        return jdbc.query("select id, first_name, last_name from customers",
                this::mapRowToPerson);
    }

    @Override
    public List<Person> findByID(long id) {
        return jdbc.query("select id, first_name, last_name from customers where id = " + id,
                this::mapRowToPerson);
    }

    @Override
    public void save(String first_name, String last_name) {
        jdbc.update("INSERT INTO customers(first_name, last_name) VALUES ('" + first_name + "', '" + last_name + "')");
    }

    /*
    @Override
    public Iterable<Person> findAll() {
        List<Person> persons = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(
                    "select id, firstName, lastName from customers");
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Person person = new Person(
                        resultSet.getString("id"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"));
                persons.add(person);
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
        return persons;
    }

    @Override
    public List<Person> findByID(long id) {
        List<Person> persons = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(
                    "select id, first_name, last_name from customers WHERE id = " + id + "");
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Person person = new Person(
                        resultSet.getString("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"));
                persons.add(person);
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
        return persons;
    }

    @Override
    public void save(String first_name, String last_name) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO customers(first_name, last_name) VALUES ('"+first_name+"', '"+last_name+"')");

        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
    }
 */
}
